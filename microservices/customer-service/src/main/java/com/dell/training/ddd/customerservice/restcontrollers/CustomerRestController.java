package com.dell.training.ddd.customerservice.restcontrollers;


import com.dell.training.ddd.customerservice.model.Customer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
public class CustomerRestController {


    @GetMapping(value = "/customers")
    public Flux<Customer> handleGetCustomers() {

        return
                Flux.interval(Duration.ofMillis(100))
                        .map(x ->
                                new Customer(x, "Customer" + x,
                                        "Address " + x))
                        .take(7)
                        .log();
    }

    @GetMapping(value = "/customers/{customerId}", produces = {MediaType.APPLICATION_STREAM_JSON_VALUE})
    public Mono<Customer> handleGetCustomerById(@PathVariable long customerId) {

        return
                Mono.just(new Customer(customerId, "Dell", "U.S"));
    }


}
