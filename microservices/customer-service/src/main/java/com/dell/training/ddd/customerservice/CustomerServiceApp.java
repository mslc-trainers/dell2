package com.dell.training.ddd.customerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CustomerServiceApp {


    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApp.class, args);
    }


}
