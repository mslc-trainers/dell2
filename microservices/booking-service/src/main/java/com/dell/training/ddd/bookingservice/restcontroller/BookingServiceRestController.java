package com.dell.training.ddd.bookingservice.restcontroller;

import com.dell.training.ddd.bookingservice.service.BookingService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BookingServiceRestController {


    @Autowired
    BookingService bookingService;

    @PostMapping(path = "/bookings")
    public Mono<Map<String, String>> handlePostBooking() {


        bookingService.createBooking();
        Map<String, String> status = new HashMap<>();
        status.put("message", "OK");

        return Mono.just(status);

    }
}
