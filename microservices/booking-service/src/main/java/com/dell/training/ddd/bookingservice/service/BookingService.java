package com.dell.training.ddd.bookingservice.service;

import com.dell.training.ddd.bookingservice.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class BookingService {

    @Autowired
    WebClient.Builder webClientBuilder;

    @Bean
    @LoadBalanced
    public WebClient.Builder loadBalancedWebClientBuilder() {
        return WebClient.builder();
    }

    public void createBooking() {

        Mono<Customer> customer = webClientBuilder
                .build()
                .get()
                .uri("http://CUSTOMER-SERVICE/customers/101")
                .retrieve()
                .bodyToMono(Customer.class);

        customer.subscribe(x -> {

            System.out.println(x);
        });


    }
}
